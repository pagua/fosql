import random
from sql import *
from collections import OrderedDict

class QueryGenConf:
    def __init__(self, schema, max_tables, max_from_tables, max_conditions):
        self.schema          = schema
        Query.schema         = schema
        self.max_tables      = max_tables
        self.max_from_tables = max_from_tables
        self.max_conditions  = max_conditions
        self.table_count     = 0
        self.nest_level      = 0
    def reset(self):
        self.table_count     = 0
        self.nest_level      = 0

def random_query(conf):
    q = Query(random_sfw(conf))
    conf.reset()
    return q

def random_sfw(conf, outer_attr=None):
    # Generate FROM list
    from_list = random_from(conf)
    from_attr = dict( [(alias,conf.schema[from_list[alias]]) for alias in from_list] )
    # Generate SELECT list
    attr_list = random_select(conf, from_list)
    # Generate WHERE clause
    num_conditions = random.randint(0, conf.max_conditions)
    if num_conditions == 0:
        return Select(attr_list, From(from_list))
    attributes = dict() if outer_attr is None else dict(outer_attr)
    attributes.update(from_attr)
    conditions = random_where_tree(conf, attributes, num_conditions).sql(config=conf, attributes=attributes)
    return Select(attr_list, From(from_list), Where(conditions))

def random_from(conf):
    max_tables = min(conf.max_tables - conf.table_count, conf.max_from_tables)
    num_tables = random.randint(1, max_tables) if max_tables > 1 else max_tables
    conf.table_count += num_tables
    from_list = OrderedDict()
    for i in range(num_tables):
        k = "T{}{}".format(conf.nest_level, i+1)
        from_list[k] = random.choice(conf.schema.keys())
    return from_list

# fails with subqueries in FROM
def random_select(conf, from_list):
    max_attributes = sum([ len(conf.schema[r]) for r in from_list.values() ])
    num_attributes = random.randint(0, max_attributes)
    attr_list = []
    for i in range(num_attributes):
        alias = random.choice(from_list.keys())
        table = from_list[alias]
        attr = random.choice(conf.schema[table])
        attr_list += ["{}.{}".format(alias,attr)]
    return attr_list

class Tree:
    def __init__(self, root, left=None, right=None):
        self.root = root
        self.left = left
        self.right = right
    def pretty_print(self, lvl=0):
        out = lvl * '\t' + str(self.root) + '\n'
        if self.left is not None:
            out += self.left.pretty_print(lvl+1)
        if self.right is not None:
            out += self.right.pretty_print(lvl+1)
        return out
    def sql(self, **kwargs):
        expr = None
        if self.root is And:
            expr = And(self.left.sql(**kwargs), self.right.sql(**kwargs))
        elif self.root is Or:
            expr = Or(self.left.sql(**kwargs), self.right.sql(**kwargs))
        elif self.root is Eq:
            expr = gen_eq(kwargs['attributes'])
        else:
            expr = self.root
        if random.choice([True, False]) is False:
            return expr
        if isinstance(expr,(And,Or,Exists)) or random.choice([True, False]) is False:
            return Not(expr)
        else:
            if isinstance(expr,Eq):
                return NotEq(Condition.pack_arg(expr.arg1),Condition.pack_arg(expr.arg2))
            else:
                return NotIn(expr.row,expr.subquery)

def random_where_tree(conf, attributes, num_leaves):
    conditions = []
    leaves = 0
    while leaves < num_leaves:
        choices = [Eq, Eq, Exists, In]
        if conf.max_tables == conf.table_count:
            choices = [Eq]
        else:
            random.shuffle(choices)
        cond = random.choice(choices)
        if cond is Exists:
            conditions.append(Tree(gen_exists(conf, attributes)))
        elif cond is In:
            conditions.append(Tree(gen_in(conf, attributes)))
        else:
            conditions.append(Tree(cond))
        leaves += 1
    connectives = []
    if num_leaves == 1:
        return conditions[0]
    while len(conditions) > 0 or len(connectives) > 1:
        conn = random.choice([And, Or])
        arg1 = random.choice(conditions + connectives)
        if arg1 in conditions:
            conditions.remove(arg1)
        else:
            connectives.remove(arg1)
        arg2 = random.choice(conditions + connectives)
        if arg2 in conditions:
            conditions.remove(arg2)
        else:
            connectives.remove(arg2)
        connectives.append(Tree(conn,arg1,arg2))
    return connectives[0]

# fails with subqueries in FROM
def gen_eq(attributes):
    rel1 = random.choice(attributes.keys())
    att1 = random.choice(attributes[rel1])
    rel2 = random.choice(attributes.keys())
    choices = [ x for x in attributes[rel2] if x != att1 ] if rel2 == rel1 else list(attributes[rel2])
    att2 = random.choice(choices)
    arg1 = "{}.{}".format(rel1,att1)
    arg2 = "{}.{}".format(rel2,att2)
    return Eq(arg1,arg2)

def gen_exists(conf, attributes):
    conf.nest_level += 1
    res = Exists(random_sfw(conf, attributes))
    conf.nest_level -= 1
    return res

# fails for subqueries without FROM
def gen_in(conf, attributes):
    conf.nest_level += 1
    subquery = random_sfw(conf, attributes)
    conf.nest_level -= 1
    row = []
    n = len(subquery.attr_list)
    n = n if n > 0 else sum([ len(conf.schema[y])
                              for x,y in subquery.from_list.tables.items() ])
    for i in range(n):
        table = random.choice(attributes.keys())
        attr = random.choice(attributes[table])
        row.append(table + '.' + attr)
    return In(row,subquery)
