from db import NULL
import itertools, copy

def is_null(x):
    return x == NULL

def is_var(x):
    try:
        if x.startswith('?'):
            return True
    except:
        return False
    return False

def replace_vars(var_list, subst):
    return [ subst[x] if x in subst.keys() else x
             for x in var_list ]

class Query:
    rename_suffix = '_'
    def __init__(self, head, body):
        self.head = head
        self.body = body
    def __str__(self):
        return "({}) : {}".format(','.join(self.head), self.body)
    def evaluate(self, db, assignment, sql3v=False):
        return self.body.evaluate(db,assignment,sql3v)
    def snnf(self):
        return Query(self.head, self.body.snnf(False))
    def sqlnf(self):
        return Query(self.head, self.body.snnf(False, True))
    def rename(self, subst, raw=False):
        for k in subst.keys():
            if subst[k] == k:
                del subst[k]
        for v in subst.values():
            if v in self.head:
                subst[v] = v + Query.rename_suffix
        return Query( replace_vars(self.head,subst),
                      self.body.rename(subst,raw) )
    def answers(self, db, sql3v=False):
        head = tuple(set(self.head))
        answers = []
        for t in itertools.product(db.adom,repeat=len(head)):
            assignment = dict(zip(head,t))
            if self.evaluate(db, assignment, sql3v) is True:
                ans = tuple([ assignment[x] if is_var(x) else x
                              for x in self.head ])
                answers.append(ans)
        return answers

class And:
    sym = '&'
    def __init__(self, arg1, arg2, *args):
        self.args = (arg1, arg2) + args
    def __str__(self):
        conjuncts = map(lambda x: '( '+str(x)+' )' if isinstance(x,Or) else str(x), self.args)
        return ' {} '.format(And.sym).join(conjuncts)
    def evaluate(self, db, assignment, sql3v=False):
        unknown = False
        for arg in self.args:
            subeval = arg.evaluate(db,assignment,sql3v)
            if subeval is False:
                return False
            if subeval is None:
                unknown = True
        return None if unknown else True
    def snnf(self, neg, sqlnf=False):
        args = map(lambda x: x.snnf(neg,sqlnf), self.args)
        return Or(*args) if neg else And(*args)
    def rename(self, subst, raw=False):
        return And(*[x.rename(subst,raw) for x in self.args])

class Or:
    sym = '|'
    def __init__(self, arg1, arg2, *args):
        self.args = (arg1, arg2) + args
    def __str__(self):
        disjuncts = map(lambda x: '( '+str(x)+' )' if isinstance(x,And) else str(x), self.args)
        return ' {} '.format(Or.sym).join(disjuncts)
    def evaluate(self, db, assignment, sql3v=False):
        unknown = False
        for arg in self.args:
            subeval = arg.evaluate(db,assignment, sql3v)
            if subeval is True:
                return True
            if subeval is None:
                unknown = True
        return None if unknown else False
    def snnf(self, neg, sqlnf=False):
        args = map(lambda x: x.snnf(neg,sqlnf), self.args)
        return And(*args) if neg else Or(*args)
    def rename(self, subst, raw=False):
        return Or(*[x.rename(subst,raw) for x in self.args])

class UnkToFalse:
    sym = '#'
    def __init__(self, arg):
        self.arg = arg
    def __str__(self):
        return '{}[ {} ]'.format(UnkToFalse.sym, str(self.arg))
    def evaluate(self, db, assignment, sql3v=False):
        subeval = self.arg.evaluate(db,assignment,sql3v)
        return False if subeval is None else subeval
    def snnf(self, neg, sqlnf=False):
        sub = self.arg.snnf(False,sqlnf)
        if neg:
            return Not(sub) if sqlnf else Not(UnkToFalse(sub))
        else:
            return sub if sqlnf else UnkToFalse(sub)
    def rename(self, subst, raw=False):
        return UnkToFalse(self.arg.rename(subst,raw))

class Not:
    sym = '~'
    def __init__(self, arg):
        self.arg = arg
    def __str__(self):
        if isinstance(self.arg, (Exists, ForAll, Rel, Not)):
            template = '{}{}'
        else:
            template = '{}( {} )'
        return template.format(Not.sym, str(self.arg))
    def evaluate(self, db, assignment, sql3v=False):
        subeval = self.arg.evaluate(db,assignment,sql3v)
        return None if subeval is None else not subeval
    def snnf(self, neg, sqlnf=False):
        return self.arg.snnf(not neg, sqlnf)
    def rename(self, subst, raw=False):
        return Not(self.arg.rename(subst,raw))

class Rel:
    def __init__(self, name, *args):
        self.name = name
        self.args = args
    def __str__(self):
        return '{}({})'.format(self.name, ','.join(self.args))
    def evaluate(self, db, assignment, sql3v=False):
        t = tuple( map(
            lambda x: assignment[x] if is_var(x) else x,
            self.args ) )
        return (t in db.data[self.name])
    def snnf(self, neg, sqlnf=False):
        rel = Rel(self.name, *self.args)
        return Not(rel) if neg else rel
    def rename(self, subst, raw=False):
        return Rel(self.name, *replace_vars(self.args,subst))

class Eq:
    sym = '='
    def __init__(self, arg1, arg2):
        self.arg1 = arg1
        self.arg2 = arg2
    def __str__(self):
        return '{}{}{}'.format(self.arg1, Eq.sym, self.arg2)
    def evaluate(self, db, assignment, sql3v=False):
        arg1, arg2 = map(lambda x: assignment[x] if is_var(x) else x, (self.arg1, self.arg2))
        if sql3v and (is_null(arg1) or is_null(arg2)):
            return None
        return arg1 == arg2
    def snnf(self, neg, sqlnf=False):
        comp = NotEq(self.arg1, self.arg2) if neg else Eq(self.arg1, self.arg2)
        return And(comp, NotNull(self.arg1), NotNull(self.arg2)) if sqlnf else comp
    def rename(self, subst, raw=False):
        return Eq(*replace_vars([self.arg1,self.arg2],subst))

class NotEq:
    sym = '!='
    def __init__(self, arg1, arg2):
        self.args = (arg1, arg2)
    def __str__(self):
        arg1, arg2 = self.args
        return '{}{}{}'.format(arg1, NotEq.sym, arg2)
    def evaluate(self, db, assignment, sql3v=False):
        eq = Eq(*self.args).evaluate(db,assignment,sql3v)
        return None if eq is None else not eq
    def snnf(self, neg, sqlnf=False):
        comp = Eq(*self.args) if neg else NotEq(*self.args)
        return And(comp, NotNull(self.arg1), NotNull(self.arg2)) if sqlnf else comp
    def rename(self, subst, raw=False):
        return NotEq(*replace_vars(self.args,subst))

class Null:
    sym = 'null'
    def __init__(self, arg):
        self.arg = arg
    def __str__(self):
        return '{}({})'.format(Null.sym, self.arg)
    def evaluate(self, db, assignment, sql3v=False):
        arg1 = self.arg
        if is_var(arg1):
            arg1 = assignment[arg1]
        return is_null(arg1)
    def snnf(self, neg, sqlnf=False):
        return NotNull(self.arg) if neg else Null(self.arg)
    def rename(self, subst, raw=False):
        return Null(self.arg.rename(subst,raw))

class NotNull:
    sym = 'not_null'
    def __init__(self, arg):
        self.arg = arg
    def __str__(self):
        return '{}({})'.format(NotNull.sym, self.arg)
    def evaluate(self, db, assignment, sql3v=False):
        return not Null(self.arg).evaluate(db,assignment,sql3v)
    def snnf(self, neg, sqlnf=False):
        return Null(self.arg) if neg else NotNull(self.arg)
    def rename(self, subst, raw=False):
        return NotNull(self.arg.rename(subst,raw))

class Exists:
    sym = '\\exists'
    def __init__(self, variables, arg):
        self.variables = variables
        self.arg = arg
    def __str__(self):
        return '{} {} ( {} )'.format(Exists.sym, ','.join(self.variables), self.arg)
    def evaluate(self, db, assignment, sql3v=False):
        unknown = False
        n = len(self.variables)
        for t in itertools.product(db.adom,repeat=n):
            assignment = copy.deepcopy(assignment)
            for i in range(n):
                assignment[self.variables[i]] = t[i]
            subeval = self.arg.evaluate(db,assignment,sql3v)
            if subeval is True:
                return True
            if subeval is None:
                unknown = True
        return None if unknown else False
    def snnf(self, neg, sqlnf=False):
        if neg:
            return ForAll(self.variables, self.arg.snnf(True,sqlnf))
        else:
            return Exists(self.variables, self.arg.snnf(False,sqlnf))
    def rename(self, subst, raw=False):
        if raw is False:
            for v in self.variables:
                subst.pop(v,None)
        for v in subst.values():
            if v in self.variables:
                subst[v] = v + Query.rename_suffix
        return Exists(replace_vars(self.variables,subst), self.arg.rename(subst,raw))

class ForAll:
    sym = '\\forall'
    def __init__(self, variables, arg):
        self.variables = variables
        self.arg = arg
    def __str__(self):
        return '{} {} ( {} )'.format(ForAll.sym, '.'.join(self.variables), self.arg)
    def evaluate(self, db, assignment, sql3v=False):
        exists = Exists(self.variables, Not(self.arg)).evaluate(db,assignment,sql3v)
        return None if exists is None else not exists
    def snnf(self, neg, sqlnf=False):
        if neg:
            return Exists(self.variables, self.arg.snnf(True,sqlnf))
        else:
            return ForAll(self.variables, self.arg.snnf(False,sqlnf))
    def rename(self, subst, raw=False):
        if raw is False:
            for v in self.variables:
                subst.pop(v,None)
        for v in subst.values():
            if v in self.variables:
                subst[v] = v + Query.rename_suffix
        return ForAll(replace_vars(self.variables,subst), self.arg.rename(subst,raw))
