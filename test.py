import psycopg2
import itertools
#from collections import Counter
from db import *
from sql import *

def sql_answers(query, pg_conn):
    cur = conn.cursor()
    cur.execute(str(query))
    ans = map(lambda r: tuple(map(lambda x: NULL if x is None else x, r)), cur.fetchall())
    cur.close()
    return ans

conn = psycopg2.connect('dbname=test host=/tmp')
conn.autocommit = True

db = Database()
db.pg_import(conn,'R','S')
#db.csv_import(open('table.csv','rb'),'R')

Query.schema = {'R': ['A'], 'S': ['A']}

q1 = Query( Select( ['R.A'],
            From({'R':'R'}),
            Where(Not(Exists( Select(['S.A'],
                              From({'S':'S'}),
                              Where(Eq('S.A','R.A'))) ))) ) )

q2 = Query( Select( ['R.A'],
            From({'R':'R'}),
            Where( Not(In( ['R.A'], Select(['S.A'],
                                    From({'S':'S'})) )) ) ) )

q3 = Query( Except( Select( ['R.A'], From({'R':'R'}) ),
                    Select( ['S.A'], From({'S':'S'}) ) ) )

q4 = Query( Select( ['R.A'],
            From({'R':'R'}),
            Where( Not(In( ['R.A'], Select( ['R.A'],
                                    From({'R':'R'}),
                                    Where( Not(In( ['R.A'], Select(['R.A'],
                                                            From({'R':'R'})) )) ) ) )) ) ) )

q5 = Query( Select( ['R.A'],
            From({'R':'R', 'J': Select(['S.A'], From({'S':'S'}))}),
            Where( Eq('J.A','R.A') ) ) ) 

fo1 = q1.fo_query()
fo2 = q2.fo_query()
fo3 = q3.fo_query()
fo4 = q4.fo_query()
fo5 = q5.fo_query()

def main():
    print '##################### Test ####################'
    print
    print '  SQL-Q1  =', q1
    print '      Q1  = {', fo1, '}'
    print ' snnf(Q1) = {', fo1.snnf(), '}'
    print 'sqlnf(Q1) = {', fo1.sqlnf(), '}'
    print
    print '  SQL-Q2  =', q2
    print '      Q2  = {', fo2, '}'
    print ' snnf(Q2) = {', fo2.snnf(), '}'
    print 'sqlnf(Q2) = {', fo2.sqlnf(), '}'
    print
    print '  SQL-Q3  =', q3
    print '      Q3  = {', fo3, '}'
    print ' snnf(Q3) = {', fo3.snnf(), '}'
    print 'sqlnf(Q3) = {', fo3.sqlnf(), '}'
    print
    print '  SQL-Q4  =', q4
    print '      Q4  = {', fo4, '}'
    print ' snnf(Q4) = {', fo4.snnf(), '}'
    print 'sqlnf(Q4) = {', fo4.sqlnf(), '}'
    print
    print '  SQL-Q5  =', q5
    print '      Q5  = {', fo5, '}'
    print ' snnf(Q5) = {', fo5.snnf(), '}'
    print 'sqlnf(Q5) = {', fo5.sqlnf(), '}'
    print
    print 'D =', db.data
    print
    print '============== SQL-3v evaluation =============='
    print
    print '      Q1 (D) =', fo1.answers(db, True)
    print '      Q2 (D) =', fo2.answers(db, True)
    print '      Q3 (D) =', fo3.answers(db, True)
    print '      Q4 (D) =', fo4.answers(db, True)
    print '      Q5 (D) =', fo5.answers(db, True)
    print
    print ' snnf(Q1)(D) =', fo1.snnf().answers(db, True)
    print ' snnf(Q2)(D) =', fo2.snnf().answers(db, True)
    print ' snnf(Q3)(D) =', fo3.snnf().answers(db, True)
    print ' snnf(Q4)(D) =', fo4.snnf().answers(db, True)
    print ' snnf(Q5)(D) =', fo5.snnf().answers(db, True)
    print
    print 'sqlnf(Q1)(D) =', fo1.sqlnf().answers(db, True)
    print 'sqlnf(Q2)(D) =', fo2.sqlnf().answers(db, True)
    print 'sqlnf(Q3)(D) =', fo3.sqlnf().answers(db, True)
    print 'sqlnf(Q4)(D) =', fo4.sqlnf().answers(db, True)
    print 'sqlnf(Q5)(D) =', fo5.sqlnf().answers(db, True)
    print
    print '============== FO naive evaluation ============'
    print
    print 'sqlnf(Q1)(D) =', fo1.sqlnf().answers(db, False)
    print 'sqlnf(Q2)(D) =', fo2.sqlnf().answers(db, False)
    print 'sqlnf(Q3)(D) =', fo3.sqlnf().answers(db, False)
    print 'sqlnf(Q4)(D) =', fo4.sqlnf().answers(db, False)
    print 'sqlnf(Q5)(D) =', fo5.sqlnf().answers(db, False)
    print
    print '============== SQL native evaluation =========='
    print
    print '   SQL-Q1(D) =', sql_answers(q1, conn)
    print '   SQL-Q2(D) =', sql_answers(q2, conn)
    print '   SQL-Q3(D) =', sql_answers(q3, conn)
    print '   SQL-Q4(D) =', sql_answers(q4, conn)
    print '   SQL-Q5(D) =', sql_answers(q5, conn)
    print
    print '###############################################'

if __name__=='__main__':
    main()
