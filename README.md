# README #

### Dependencies ###

* [Python 2.7](https://www.python.org/downloads/release/python-2711/) with [Psycopg2](http://initd.org/psycopg/) module
* [PostgreSQL](https://www.postgresql.org/)
* [Datafiller](https://www.cri.ensmp.fr/people/coelho/datafiller.html)

### Configuration ###

Create a new empty database (we will assume this is called `test`, but you can give it any name you like) with the command:
```
createdb test
```

### Generating test data ###

The command
```
datafiller --drop -n 0.33 schema.sql | psql test
```
will generate random data and populate the PostgreSQL database `test` with it.

If you want to save the generated data to file, use:
```
datafiller --drop -n 0.33 schema.sql > data.sql
```
You can then populate the database with the command:
```
psql test -f data.sql
```

### Running the tests ###

The command:
```
python fosql.py test
```
will generate random SQL queries, execute each of them on the data stored in the PostgreSQL database `test` and compare their output with the answers to the corresponding FO translation. By default the number of generated queries is 10, but this can be changed with the option `-q`.

To see all available options and arguments accepted by `fosql.py`, run the command:
```
python fosql.py --help
```
