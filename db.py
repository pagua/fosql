import csv

NULL = 'N'

class Database:
    def __init__(self, data=None, adom=None):
        if data:
            self.data = data
            if adom:
                self.adom = adom
            else:
                self.adom = self.compute_adom()
        else:
            self.data = dict()
            self.adom = set()

    def compute_adom(self):
        self.adom = reduce(lambda x,y: set(x).union(set(y)), reduce(lambda x,y: x+y, self.data.values()))

    def pg_import(self, pg_conn, *relnames):
        for rel in relnames:
            cur = pg_conn.cursor()
            cur.execute("SELECT * FROM {}".format(rel))
            self.data[rel] = map(lambda r: tuple(map(lambda x: NULL if x is None else x, r)), cur.fetchall())
            cur.close()
        self.compute_adom()

    def csv_import(self, csvfile, relname):
        self.data[relname] = map(lambda r: tuple(map(lambda x: NULL if x=='' else int(x), r )), csv.reader(csvfile) )
        self.compute_adom()
