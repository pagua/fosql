import psycopg2
from fo import *
from db import *
from gen import *
from collections import Counter
from multiprocessing import Pool
import logging
import psycopg2
import subprocess
import argparse
import sys

def parse_args():
    parser = argparse.ArgumentParser(add_help=False,
        description='Compare output of randomly generated SQL queries and their FO translations.')
    parser.add_argument( '--version', action='version',
                         version='%(prog)s v0.1 by Paolo Guagliardo' )
    parser.add_argument( '--help', action='help',
                         help="print this help message and exit" )
    parser.add_argument( 'dbname', metavar='<dbname>', type=str,
                         help=("PostgreSQL database where test data is stored") )
    parser.add_argument( '-u', '--user', metavar='<username>', type=str,
                         help="connect to the database as '%(metavar)s'" )
    parser.add_argument( '-h', '--host', metavar='<hostname>', type=str,
                         help="the machine on which the server is running" )
    parser.add_argument( '-p', '--port', metavar='<port>', type=str,
                         help="the port on which the server is listening" )
    parser.add_argument( '-v', '--verbose', action='store_true',
                         help="verbose output" )
    parser.add_argument( '-q', '--queries', metavar='INT', type=int, default=10,
                         help="Number of random queries to be generated [default: %(default)s]" )
    args = parser.parse_args()
    # validate arguments here ...
    return args

def sql_answers(query, pg_conn):
    cur = pg_conn.cursor()
    cur.execute(str(query))
    ans = map(lambda r: tuple(map(lambda x: NULL if x is None else x, r)), cur.fetchall())
    cur.close()
    return ans

def compare_ans(q, conn_str, db, verbose=False):
    conn = psycopg2.connect(conn_str)
    conn.autocommit = True
    fo = q.fo_query()
    sql_ans = sql_answers(q, conn)
    fo_ans = fo.answers(db, sql3v=True)
    conn.close()
    if verbose:
        msg = "Verifying query\n>>> SQL\n{}\n>>> FO\n{}\n"
        logging.info(msg.format(q,fo))
    return Counter(sql_ans) == Counter(fo_ans)

def f(x):
    return compare_ans(*x)

def main():
    log_format = "%(asctime)s [%(levelname)-6s] %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_format)
    args = parse_args()

    # connect to the database and load the specified instance
    conn_str = 'dbname=' + args.dbname
    if args.user is not None:
        conn_str += (' user=' + args.user)
    if args.host is not None:
        conn_str += (' host=' + args.host)
    if args.port is not None:
        conn_str += (' port=' + args.port)
    conn = psycopg2.connect(conn_str)
    conn.autocommit = True

    d = dict([ (r,['A', 'B']) for r in [ 'R1', 'R2', 'R3', 'R4', 'R5' ] ])
    db = Database()
    db.pg_import(conn,*d.keys())
    conn.close()

    gen_conf = QueryGenConf(d,5,3,4) # these parameters should be read from stdin or automatically generated
    queries = [ random_query(gen_conf) for i in range(args.queries) ]

    fail = 0
    success = 0

    p = Pool(4)
    seq = [ (q,conn_str,db,args.verbose) for q in queries ]
    results = p.map(f, seq)
    success = len([ x for x in results if x is True ])
    fails = len([ x for x in results if x is False ])
    msg = "Verified {} queries: {} OK / {} failed / {} skipped"
    print msg.format(len(queries), success, fail, len(queries)-len(results))

if __name__ == "__main__":
    main()
